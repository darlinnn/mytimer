package org.example;

import java.util.Date;

public interface Work {
	public Date getStartTime();
	public Date getEndTime();
}
