package org.example;

import android.content.Context;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtils {
	private static SimpleDateFormat dateFomat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
	private static SimpleDateFormat lengthSecFomat = new SimpleDateFormat("ss");
	private static SimpleDateFormat lengthMinSecFomat = new SimpleDateFormat("mm:ss");
	private static SimpleDateFormat lengthHrsMinSecFomat = new SimpleDateFormat("hh:mm:ss");
	private static SimpleDateFormat timeFomat = new SimpleDateFormat("HH:mm");

	private static Calendar calendar = new GregorianCalendar(TimeZone.getDefault());
	private static Calendar deltaCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
	
	public static Date dateFromString(String dateString){
	    Date date = null;
		try {
			date = dateFomat.parse(dateString);
		} catch (ParseException e) {
			Log.e("CC", "Can't parse date");
		}
	    return date;
	}

	public static String dateToString(Date date){
	    return (date == null) ? "" : dateFomat.format(date);
	}

	public static String formatWorkLength(Date endTime, Date startTime, Context context) {
		return formatWorkLength(getWorkLength(endTime, startTime), context);
	}

	public static String formatWorkLength(long length, Context context) {
		String str = "";
		if (length >= 0){
			deltaCalendar.setTimeInMillis(length);
			int sec = deltaCalendar.get(Calendar.SECOND);
			int min = deltaCalendar.get(Calendar.MINUTE);
			str = (min > 0) ? lengthMinSecFomat.format(deltaCalendar.getTime())
					: "" + sec + " " + context.getResources().getString(R.string.sec_label);
		}
		return str;
	}

	public static long getWorkLength(Date endTime, Date startTime) {
		long result = -1;
		if ((endTime != null) && (startTime != null)){
			deltaCalendar.setTimeInMillis(endTime.getTime() - startTime.getTime());
			result = deltaCalendar.getTimeInMillis();
		}
		return result;
	}

	public static String formatRelaxLength(long length) {
		String str = "";
		if (length >= 0){
			deltaCalendar.setTimeInMillis(length);
            calendar.set(Calendar.HOUR_OF_DAY, deltaCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, deltaCalendar.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, deltaCalendar.get(Calendar.SECOND));
            Date timeToFormat = calendar.getTime();

            str = deltaCalendar.get(Calendar.HOUR_OF_DAY) > 0 ? lengthHrsMinSecFomat.format(timeToFormat)
					: lengthMinSecFomat.format(timeToFormat);
		}
		return str;
	}

	public static String formatRelaxLength(Date startTime, Date prevEndTime) {
		return formatRelaxLength(getRelaxLength(startTime, prevEndTime));
	}

	public static long getRelaxLength(Date startTime, Date prevEndTime) {
		long result = -1;
		if ((prevEndTime != null) && (startTime != null)){
			deltaCalendar.setTimeInMillis(startTime.getTime() - prevEndTime.getTime());
			result = deltaCalendar.getTimeInMillis();
		}
		return result;
	}

	public static String formatTime(Date time) {
		return timeFomat.format(time); 
	}

	public static int getHours(Date startTime) {
		calendar.setTime(startTime);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMins(Date startTime) {
		calendar.setTime(startTime);
		return calendar.get(Calendar.MINUTE);
	}

	public static Date getUpdatedDateWithHoursAndMin(Date time, int hours, int min) {
		calendar.setTime(time);
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, min);
		return calendar.getTime();
	}

/*
	public static Date getUpdatedDateWithDelta(Date time, long delta) {
		calendar.setTime(time);
		calendar.add(Calendar.MILLISECOND, (int) delta);
		return calendar.getTime();
	}

	public static long getDelta(Date oldDate, int newHours, int newMin) {
		deltaCalendar.setTime(oldDate);
		long oldTime = deltaCalendar.getTimeInMillis();
		
		deltaCalendar.set(Calendar.HOUR_OF_DAY, newHours);
		deltaCalendar.set(Calendar.MINUTE, newMin);
		return deltaCalendar.getTimeInMillis() - oldTime;
	}
*/

}
