package org.example;

import java.util.Date;

public class WorkEntry {

	private Date startTime;
	private Date endTime;
	public WorkEntry(Date startTime) {
		super();
		this.startTime = startTime;
	}
	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	public Date getEndTime() {
		return endTime;
	}
	
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void updateWithNewStartTime(int hours, int min) {
//		this.endTime = DateUtils.getUpdatedDateWithDelta(endTime, DateUtils.getDelta(startTime, hours, min));
		this.startTime = DateUtils.getUpdatedDateWithHoursAndMin(startTime, hours, min);
	}

    public void updateWithNewEndTime(int hours, int min) {
        this.endTime = DateUtils.getUpdatedDateWithHoursAndMin(endTime, hours, min);
    }
}
