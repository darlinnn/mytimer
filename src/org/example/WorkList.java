package org.example;

import android.content.Context;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class WorkList {
	private static final String END_JSON_NAME = "End";
	private static final String START_JSON_NAME = "Start";
	private static final String WorkS_JSON_NAME = "Works";
	
	private List<WorkEntry> Works = new LinkedList<WorkEntry>();
	private boolean isWorkNow;

	public boolean isWorkNow() {
		return isWorkNow;
	}

	public List<WorkEntry> getWorks() {
		return Works;
	}

	public static WorkList fromJson(String jsonString) {
		WorkList WorkList = new WorkList();
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(jsonString);
			JSONArray WorksJson = jsonObject.getJSONArray(WorkS_JSON_NAME);
			for (int i = 0; i < WorksJson.length(); i++){
				JSONObject jsonWork = (JSONObject) WorksJson.get(i);
				
				Date startDate = DateUtils.dateFromString(jsonWork.getString(START_JSON_NAME));
				Date endDate = DateUtils.dateFromString(jsonWork.getString(END_JSON_NAME));
				
				WorkEntry WorkEntry = new WorkEntry(startDate);
				WorkEntry.setEndTime(endDate);
				
				WorkList.add(WorkEntry);
			}
		} catch (JSONException e) {
			Log.e("CC", "Deserialize error");
		}
		return WorkList;
	}
	
	public String toJson(){
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonWorks = new JSONArray();
		try {
			for (WorkEntry WorkEntry : Works){
				JSONObject jsonWork = new JSONObject();
				jsonWork.put(START_JSON_NAME, DateUtils.dateToString(WorkEntry.getStartTime()));
				jsonWork.put(END_JSON_NAME, DateUtils.dateToString(WorkEntry.getEndTime()));
				jsonWorks.put(jsonWork);
			}
			jsonObject.put(WorkS_JSON_NAME, jsonWorks);
		} catch (JSONException e) {
			Log.e("CC", "Serialize error");
		}
		Log.d("CC", jsonObject.toString());
		return jsonObject.toString();
	}
	
	public void remove(int i){
		Works.remove(i);
	}

	public void removeAll() {
		Works.removeAll(Works);
	}
	
	public void add(WorkEntry WorkEntry) {
		Works.add(WorkEntry);
	}

	public int size() {
		return Works.size();
	}

	public WorkEntry getLastWork() {
		return Works.size() > 0 ? Works.get(Works.size() - 1) : new WorkEntry(new Date());
	}

	public String getWorkStart(int i) {
		return DateUtils.formatTime(Works.get(i).getStartTime());
	}

	public String getFormattedStartedTime() {
		String str = "";
		if (Works.size() > 0){
			str = DateUtils.formatTime(Works.get(0).getStartTime());
		}
		return str;
	}

	public String getFormattedWorkLength(int i, Context context) {
		return DateUtils.formatWorkLength(Works.get(i).getEndTime(), Works.get(i).getStartTime(), context);
	}
	
	public String getFormattedPrevRelax(int i) {
		String str = "";
		if ((Works.size() > 1) && (i > 0 )){
			str = DateUtils.formatRelaxLength(Works.get(i).getStartTime(), Works.get(i - 1).getEndTime());
		}
		return str;
	}

	private long getWorkLength(int i) {
		return DateUtils.getWorkLength(Works.get(i).getEndTime(), Works.get(i).getStartTime());
	}
	
	private long getPrevRelax(int i) {
		long res = 0;
		if ((Works.size() > 1) && (i > 0 )){
			res = DateUtils.getRelaxLength(Works.get(i).getStartTime(), Works.get(i - 1).getEndTime());
		}
		return res;
	}

	public String getAverageWorkTime(Context context) {
		long sum = 0;
		int count = 0;
		for (int i = 0; i < Math.min(Works.size(), 4); i++) {
			sum += getWorkLength(Works.size() - i - 1);
			if (getWorkLength(Works.size() - i - 1) > 0){
				count ++;
			}
		}		
		return (count > 0) ? DateUtils.formatWorkLength(sum / count, context) : "";
	}

	public String getAverageRelaxTime() {
		long sum = 0;
		int count = 0;
		for (int i = 0; i < Math.min(Works.size(), 5); i++) {
			sum += getPrevRelax(Works.size() - i - 1);
			if (getPrevRelax(Works.size() - i - 1) > 0){
				count ++;
			}
		}
		return (count > 0) ? DateUtils.formatRelaxLength(sum / count) : "";
	}
}
