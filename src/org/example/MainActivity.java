package org.example;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.*;

import java.util.Date;

public class MainActivity extends ListActivity {
	public static String SHARED_PREF_NAME = "WorkS_SP";
	public static String WorkS_SP_NAME = "WorkS";
	public static String IS_Work_NOW_SP_NAME  = "IS_Work_NOW";

    private Button button; 
	private TextView summaryStartTextView;
	private TextView summaryAverageWorkTextView;
	private TextView summaryAverageRelaxTextView;
	private Chronometer chronometer;

    private boolean isWorkNow;
    private SharedPreferences sharedPreferences;
    
    private WorkAdapter WorkAdapter;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WorkAdapter = new WorkAdapter();
        setListAdapter(WorkAdapter);
        registerForContextMenu(getListView());
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(buttonClickListener);

        summaryStartTextView = (TextView)findViewById(R.id.started_at);
    	summaryAverageWorkTextView = (TextView)findViewById(R.id.avg_work);
    	summaryAverageRelaxTextView = (TextView)findViewById(R.id.avg_relax);

        chronometer = (Chronometer)findViewById(R.id.chronometer);
    
        sharedPreferences = getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        initFromSharedPreferences();
    }

    private OnClickListener buttonClickListener = new OnClickListener() {
        public void onClick(View v) {
        	WorkAdapter.updateWorkList(isWorkNow);
        	isWorkNow = !isWorkNow;
        	setWorkNow(isWorkNow);
         }
    };

	private void initFromSharedPreferences() {
    	isWorkNow = sharedPreferences.getBoolean(IS_Work_NOW_SP_NAME, false);
    	updateButtonText(isWorkNow);
    	WorkAdapter.loadSavedWorks();
    }
	private void setWorkNow(boolean isWorkNow) {
        saveIsWorkState(isWorkNow);
        updateButtonText(isWorkNow);
	}

	private void saveIsWorkState(boolean isWorkNow) {
		Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_Work_NOW_SP_NAME, isWorkNow);
        editor.commit();
	}

	@SuppressLint("NewApi")
    private void updateButtonText(boolean isWorkNow) {
        button.setText(isWorkNow ? R.string.stop : R.string.start);
        button.setTextColor(Color.parseColor(isWorkNow ? "#095e17" : "#6d242d"));
        button.setBackground(this.getResources().getDrawable(isWorkNow ? R.drawable.btn_green_matte : R.drawable.btn_pink_matte));
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);


        getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
    	AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		    case R.id.menu_edit_start:
		    	WorkAdapter.editStart(menuInfo.position);
   		    	break;
		    case R.id.menu_edit_end:
		    	WorkAdapter.editEnd(menuInfo.position);
   		    	break;
		    case R.id.menu_remove:
				WorkAdapter.remove(menuInfo.position);
		    	break;
		    default:
			    return super.onContextItemSelected(item);
		}
		return true;
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		    case R.id.menu_remove_all:
                new AlertDialog.Builder(this)
                        .setMessage(R.string.confirm_remove_all)
                        .setCancelable(false)
                        .setPositiveButton(R.string.confirm_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                WorkAdapter.removeAll();
                            }
                        })
                        .setNegativeButton(R.string.confirm_no, null)
                        .show();
			    break;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	
	
	public class WorkAdapter extends BaseAdapter {

		private WorkList WorkList= new WorkList();
		private int editingIndex;

		public int getCount() {
			return WorkList.size();
		}

		public Object getItem(int position) {
			return WorkList.getWorks().get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
			LinearLayout row = (LinearLayout) inflater.inflate(R.layout.row_layout, null);		
			addTextToRow(0, row, "  " + WorkList.getWorkStart(position) + "  ");
			addTextToRow(1, row, "  " + WorkList.getFormattedWorkLength(position, getApplicationContext()) + "  ");
			addTextToRow(2, row, "  " + WorkList.getFormattedPrevRelax(position) + "  ");
	    	return row;
		}
	
	  	private void addTextToRow(int index, LinearLayout row, String value) {
			TextView textView = (TextView) row.getChildAt(index);
			textView.setText(value);
		}

		public void updateWorkList(boolean isWorkNow) {
	    	if (isWorkNow){
				WorkList.getLastWork().setEndTime(new Date());
	    	} else {
				WorkList.add(new WorkEntry(new Date()));
	    	}
	    	updateTimer(isWorkNow);
			notifyDataSetChanged();
		}

		public void loadSavedWorks() {
			String WorksJson = sharedPreferences.getString(WorkS_SP_NAME, "");
			WorkList = WorkList.fromJson(WorksJson);
			notifyDataSetChanged();
		}
		
		private void removeAll() {
			WorkList.removeAll();
			hideTimer();
			notifyDataSetChanged();
		}

		@Override
		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
			serializeWorkList();
			updateSummary();
		}
		
		private void serializeWorkList() {
			String jsonString = WorkList.toJson();
	    	Editor editor = sharedPreferences.edit();
			editor.putString(WorkS_SP_NAME, jsonString);
			editor.commit();
		}

		private void updateSummary() {
			summaryStartTextView.setText(WorkList.getFormattedStartedTime());
			summaryAverageWorkTextView.setText(WorkList.getAverageWorkTime(getApplicationContext()));
			summaryAverageRelaxTextView.setText(WorkList.getAverageRelaxTime());
		}
		
		private void remove(int index) {
			WorkList.remove(index);
			notifyDataSetChanged();
		}

        private void editStart(int index) {
            WorkEntry editingWork = WorkList.getWorks().get(index);
            editingIndex = index;
            Date startTime = editingWork.getStartTime();
            TimePickerDialog tpd = new TimePickerDialog(MainActivity.this, startTimeChangedCallback, DateUtils.getHours(startTime), DateUtils.getMins(startTime), true);
            tpd.show();
        }

        OnTimeSetListener startTimeChangedCallback = new OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hours, int min) {
                updateStartIfPossible(hours, min);
                WorkAdapter.notifyDataSetChanged();
				WorkAdapter.serializeWorkList();
				WorkAdapter.loadSavedWorks();
			}
		};

        private void updateStartIfPossible(int hours, int min) {
            WorkEntry editingEntry = WorkAdapter.WorkList.getWorks().get(editingIndex);
            if (checkPrevBoundaryForStartTime(editingEntry, hours, min) && checkNextBoundaryForStartTime(editingEntry, hours, min)) {
                editingEntry.updateWithNewStartTime(hours, min);
            }
        }

        private void editEnd(int index) {
            WorkEntry editingWork = WorkList.getWorks().get(index);
            editingIndex = index;
            Date endTime = editingWork.getEndTime();
            if (endTime != null) {
                TimePickerDialog tpd = new TimePickerDialog(MainActivity.this, endTimeChangedCallback, DateUtils.getHours(endTime), DateUtils.getMins(endTime), true);
                tpd.show();
            }
        }

        OnTimeSetListener endTimeChangedCallback = new OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hours, int min) {
                updateEndIfPossible(hours, min);

                WorkAdapter.notifyDataSetChanged();
                WorkAdapter.serializeWorkList();
                WorkAdapter.loadSavedWorks();
            }
        };

        private boolean checkPrevBoundaryForStartTime(WorkEntry editingEntry, int hours, int min) {
            if (editingIndex > 0) {
                WorkEntry prevEntry = WorkAdapter.WorkList.getWorks().get(editingIndex - 1);
                if (DateUtils.getUpdatedDateWithHoursAndMin(editingEntry.getStartTime(), hours, min).after(prevEntry.getEndTime())) {
                    return true;
                }
                Toast.makeText(getApplicationContext(), R.string.cant_edit_label, Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }

        private boolean checkNextBoundaryForStartTime(WorkEntry editingEntry, int hours, int min) {
            if (DateUtils.getUpdatedDateWithHoursAndMin(editingEntry.getStartTime(), hours, min).before(editingEntry.getEndTime())){
                return true;
            }
            Toast.makeText(getApplicationContext(), R.string.cant_edit_label, Toast.LENGTH_SHORT).show();
            return false;

        }

        private void updateEndIfPossible(int hours, int min) {
            WorkEntry editingEntry = WorkAdapter.WorkList.getWorks().get(editingIndex);
            if (checkPrevBoundaryForEndTime(editingEntry, hours, min) && checkNextBoundaryForEndTime(editingEntry, hours, min)){
                editingEntry.updateWithNewEndTime(hours, min);
            }
        }

        private boolean checkPrevBoundaryForEndTime(WorkEntry editingEntry, int hours, int min) {
            if (DateUtils.getUpdatedDateWithHoursAndMin(editingEntry.getEndTime(), hours, min).after(editingEntry.getStartTime())) {
                return true;
            }
            Toast.makeText(getApplicationContext(), R.string.cant_edit_label, Toast.LENGTH_SHORT).show();
            return false;
        }

        private boolean checkNextBoundaryForEndTime(WorkEntry editingEntry, int hours, int min) {
            Date maxTime = new Date();
            if (editingIndex < (WorkAdapter.WorkList.getWorks().size() - 1)){
                WorkEntry nextEntry = WorkAdapter.WorkList.getWorks().get(editingIndex + 1);
                maxTime = nextEntry.getStartTime();
            }
            if (DateUtils.getUpdatedDateWithHoursAndMin(editingEntry.getEndTime(), hours, min).before(maxTime)) {
                return true;
            }
            Toast.makeText(getApplicationContext(), R.string.cant_edit_label, Toast.LENGTH_SHORT).show();
            return false;
        }

		private void hideTimer() {
			chronometer.stop();
			chronometer.setVisibility(View.INVISIBLE);
		}

		private void updateTimer(boolean isWorkNow) {
			chronometer.stop();
			chronometer.setVisibility(View.VISIBLE);
			chronometer.setBase(SystemClock.elapsedRealtime());
			chronometer.setTextColor(Color.parseColor(isWorkNow ? "#095e17" : "#6d242d"));
            chronometer.start();
		}
	}
}
